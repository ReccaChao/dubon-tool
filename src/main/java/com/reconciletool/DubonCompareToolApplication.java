package com.reconciletool;

import com.reconciletool.model.ReconcileDTO;
import com.reconciletool.service.impl.ReconcileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.ConfigurableEnvironment;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@SpringBootApplication
public class DubonCompareToolApplication implements CommandLineRunner {

    @Autowired
    private ConfigurableEnvironment env;

    @Autowired
    private ReconcileService reconcileService;

    private static Logger logger = LoggerFactory.getLogger(DubonCompareToolApplication.class);
    String path = System.getProperty("user.dir");
    Path pathMof = Paths.get(path + "\\mof");
    Path pathSap = Paths.get(path + "\\sap");
    Path pathResult = Paths.get(path + "\\result");
    static String[] header = new String[]{"invoiceNumber", "salesAmount", "salesAmountDiff", "vatAmount", "vatSalesAmountDiff"};
    static String[] displayHeader = new String[]{"Invoice No. Comparison 1", "Sales Amount Comparison 1", "Sales Amount Variance 1", "VAT Amount Comparison 1", "VAT Amount Variance 1"};

    public static void main(String[] args) {
        SpringApplication.run(DubonCompareToolApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        // ===== step 1.check folder exist & create mof,sap,result folder
        reconcileService.createFolder(pathMof, pathSap, pathResult);

        // ===== step 2.load data
        List<ReconcileDTO> mofList;
        List<ReconcileDTO> sapList;
        try {
            mofList = reconcileService.genReconcileList(pathMof, "mof");
            sapList = reconcileService.genReconcileList(pathSap, "sap");
        } catch (Exception e) {
            mofList = null;
            sapList = null;
        }

        //  ===== step 3.export result to result folder
        if (mofList != null && sapList != null) {
            Map<String, ReconcileDTO> mofMap = mofList.stream().collect(Collectors.toMap(ReconcileDTO::getInvoiceNumber, Function.identity()));
            Map<String, ReconcileDTO> sapMap = sapList.stream().collect(Collectors.toMap(ReconcileDTO::getInvoiceNumber, Function.identity()));
            //based on Mof excel
            reconcileService.export(mofList, sapMap, "以財政部為基礎，比對SAP數據.xls", displayHeader, header, pathResult.toString());
            //based on SAP excel
            reconcileService.export(sapList, mofMap, "以SAP為基礎,比對財政部數據.xls", displayHeader, header, pathResult.toString());
            logger.info("export SUCCESS!! after 5 second close console...");
            Thread.sleep(5000);
        } else {
            logger.error("export FAIL!! after 50 second close console...");
            logger.error("mof list & sap list is not exist!");
            Thread.sleep(50000);
        }


    }


}
