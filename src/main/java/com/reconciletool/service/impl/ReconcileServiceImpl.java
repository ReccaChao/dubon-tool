package com.reconciletool.service.impl;

import com.aspose.cells.Row;
import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;
import com.reconciletool.DubonCompareToolApplication;
import com.reconciletool.model.ReconcileDTO;
import com.reconciletool.utils.AsposeCellUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ReconcileServiceImpl implements ReconcileService {

    private static Logger logger = LoggerFactory.getLogger(ReconcileServiceImpl.class);

    @Override
    public List<ReconcileDTO> genReconcileList(Path path, String type) throws Exception {
        Workbook workbookA = new Workbook(getInputStreamByPath(path));
        Worksheet worksheet = workbookA.getWorksheets().get(0);
        if ("mof".equals(type))
            worksheet.getCells().deleteRows(0, 2, true);
        if ("sap".equals(type))
            worksheet.getCells().deleteRows(0, 2, true);
        logger.info("{} file total count: {}", type, worksheet.getCells().getRows().getCount());
        List<ReconcileDTO> reconcileDTOList = new ArrayList<>();
        for (Object o : worksheet.getCells().getRows()) {
            ReconcileDTO reconcileDTO = new ReconcileDTO();
            Row rowData = (Row) o;
            if ("mof".equals(type)) {
                reconcileDTO.setInvoiceNumber(rowData.get(4).getValue().toString());
                reconcileDTO.setSalesAmount(new BigDecimal((Integer) rowData.get(11).getValue()));
                reconcileDTO.setVatAmount(new BigDecimal((Integer) rowData.get(13).getValue()));
            }
            if ("sap".equals(type)) {
                reconcileDTO.setInvoiceNumber(rowData.get(0).getValue().toString());
                reconcileDTO.setSalesAmount(new BigDecimal((Integer) rowData.get(10).getValue()));
                reconcileDTO.setVatAmount(new BigDecimal((Integer) rowData.get(13).getValue()));
            }
            reconcileDTOList.add(reconcileDTO);
        }
        return reconcileDTOList;
    }


    public void listToWorkBook(List<ReconcileDTO> list, String path, String fileName,String[] displayHeader,String[] header) throws IOException {
        if (list != null && list.size() > 0) {
            try {
                // 設定一個預設資料夾路徑
                File file = new File(path);
                // 如果該目錄不存在,則建立之
                if (file.exists() == false) {
                    file.mkdirs();
                }
                // Obtaining Worksheet's cells collection
                Workbook workbook = AsposeCellUtils.generateSorted(displayHeader, header, list);
                workbook.getWorksheets().get(0).setName(fileName.replace(".xls",""));
                workbook.save(path + "\\" + fileName);
            } catch (IOException e) {
                throw new IOException("匯出excel失敗:" + e.getMessage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void export(List<ReconcileDTO> list, Map<String, ReconcileDTO> sapMap, String fileName, String[] displayHeader, String[] header,String pathResult) throws IOException {
        for (ReconcileDTO mof : list.stream().collect(Collectors.toSet())) {
            try {
                BigDecimal salesAmountDiff = null;
                BigDecimal vatAmountDiff = null;
                ReconcileDTO sap = sapMap.get(mof.getInvoiceNumber());
                salesAmountDiff = mof.getSalesAmount().subtract(sap.getSalesAmount()).compareTo(BigDecimal.ZERO) != 0 ? mof.getSalesAmount().subtract(sap.getSalesAmount()) : BigDecimal.ZERO;
                vatAmountDiff = mof.getVatAmount().subtract(sap.getVatAmount()).compareTo(BigDecimal.ZERO) != 0 ? mof.getVatAmount().subtract(sap.getVatAmount()) : BigDecimal.ZERO;
                mof.setSalesAmountDiff(salesAmountDiff);
                mof.setVatSalesAmountDiff(vatAmountDiff);
            } catch (Exception e) {

            }
        }
        //export
        listToWorkBook(list, pathResult, fileName, displayHeader, header);
        //logger.info("export list count:{}", list.size());
    }

    public void createFolder(Path pathMof,Path pathSap,Path pathResult) {
        try {
            if (!Files.exists(pathMof)) {
                Files.createDirectory(pathMof);
            }
            if (!Files.exists(pathSap)) {
                Files.createDirectory(pathSap);
            }
            if (Files.exists(pathResult)) {
                Files.walk(pathResult)
                        .sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        .forEach(File::delete);
                Files.createDirectory(pathResult);
            } else {
                Files.createDirectory(pathResult);
            }
            logger.info("folder is exist");
        } catch (IOException e) {
            logger.error("folder is not exist");
            e.printStackTrace();
        }
    }

    InputStream getInputStreamByPath(Path path) {
        try (Stream<Path> paths = Files.walk(path)) {
            Optional<Path> first = paths.filter(Files::isRegularFile).findFirst();
            InputStream inputStream = Files.newInputStream(first.get());
            return inputStream;
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return null;
    }

}
