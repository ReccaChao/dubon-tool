package com.reconciletool.model;

import java.math.BigDecimal;

public class ReconcileDTO {

    private String invoiceNumber;
    private BigDecimal salesAmount;
    private BigDecimal vatAmount;
    private BigDecimal salesAmountDiff;
    private BigDecimal vatSalesAmountDiff;

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public BigDecimal getSalesAmount() {
        return salesAmount;
    }

    public void setSalesAmount(BigDecimal salesAmount) {
        this.salesAmount = salesAmount;
    }

    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    public BigDecimal getSalesAmountDiff() {
        return salesAmountDiff;
    }

    public void setSalesAmountDiff(BigDecimal salesAmountDiff) {
        this.salesAmountDiff = salesAmountDiff;
    }

    public BigDecimal getVatSalesAmountDiff() {
        return vatSalesAmountDiff;
    }

    public void setVatSalesAmountDiff(BigDecimal vatSalesAmountDiff) {
        this.vatSalesAmountDiff = vatSalesAmountDiff;
    }
}
