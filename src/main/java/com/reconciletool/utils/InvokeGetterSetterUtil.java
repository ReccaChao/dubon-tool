package com.reconciletool.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class InvokeGetterSetterUtil {

  private static Logger logger = LoggerFactory.getLogger(InvokeGetterSetterUtil.class);

  //反射 setter
  public static void invokeSetter(Object obj, String propertyName, Object value) {
    if (obj == null) {
      return;
    }
    //若為以下型別，直接 return，不須反射 set
    if (obj instanceof String
        || obj instanceof Integer
        || obj instanceof Float
        || obj instanceof BigDecimal
        || obj instanceof Date
        || obj instanceof LocalDate
        || obj instanceof LocalDateTime
        || obj instanceof Boolean) {
      return;
    }
    //若為物件，透過反射，找物件裡對應的屬性，並設值
    final PropertyDescriptor pd = BeanUtils.getPropertyDescriptor(obj.getClass(), propertyName);
    final Method setter = pd.getWriteMethod();
    try {
      setter.invoke(obj, value);
    } catch (Throwable e) {
      logger.error(e.getMessage());
    }
  }

  //反射 getter
  public static Object invokeGetter(Object obj, String propertyName) {
    if (obj == null) {
      return null;
    }
    //若為以下型別，直接 return，因為只有單個值，不含其他屬性
    if (obj instanceof String
        || obj instanceof Integer
        || obj instanceof Float
        || obj instanceof BigDecimal
        || obj instanceof Date
        || obj instanceof LocalDate
        || obj instanceof LocalDateTime
        || obj instanceof Boolean) {
      return obj;
    }
    //若為物件，透過反射，找物件裡對應的屬性值
    Object result = obj;
    try {
      final PropertyDescriptor pd = BeanUtils.getPropertyDescriptor(obj.getClass(), propertyName);
      final Method getter = pd.getReadMethod();
      result = getter.invoke(obj);
    } catch (Throwable e) {
      logger.error(e.getMessage());
    }
    return result;
  }

}
